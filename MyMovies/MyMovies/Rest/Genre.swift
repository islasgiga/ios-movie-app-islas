//
//  Genre.swift
//  MyMovies
//
//  Created by Islas Girão Garcia on 08/06/2018.
//  Copyright © 2018 Islas Girão Garcia. All rights reserved.
//

import Foundation


class Genre{
    
    class func genreBy(id: Int){
        switch(id){
        case 28:
            return "Action"
            
        case 12:
            return "Adventure"
            
        case 16:
            return "Animation"
            
        case 35:
            return "Comedy"
            
        case 80:
            return "Crime"
            
        case 99:
            return "Documentary"
            
        case 18:
            return "Drama"
            
        case 10751:
            return "Family"
            
        case 14:
            return"Fantasy"
            
        case 36:
            return "History"
        }
        
        case 27:
            return "Horror"
        
        case 10402:
            return "Music"
        
        "id": 9648,
        "name": "Mystery"
        
        "id": 10749,
        "name": "Romance"
        
        "id": 878,
        "name": "Science Fiction"
        
        "id": 10770,
        "name": "TV Movie"
        
        "id": 53,
        "name": "Thriller"
        
        "id": 10752,
        "name": "War"
        
        "id": 37,
        "name": "Western"
        
    }
}


