//
//  FavoriteMovieTableViewCell.swift
//  MyMovies
//
//  Created by Islas Girão Garcia on 07/06/2018.
//  Copyright © 2018 Islas Girão Garcia. All rights reserved.
//

import UIKit

class FavoriteMovieTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
