//
//  FavoritesViewController.swift
//  MyMovies
//
//  Created by Islas Girão Garcia on 07/06/2018.
//  Copyright © 2018 Islas Girão Garcia. All rights reserved.
//

import UIKit

class FavoritesMoviesViewController: UIViewController {

    @IBOutlet weak var tableFavoriteMovies: UITableView!
    var movies: [FavoriteMovie]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableFavoriteMovies.delegate = self
        self.tableFavoriteMovies.dataSource = self
        self.tableFavoriteMovies.register(UINib(nibName: "FavoriteMovieTableViewCell", bundle: nil), forCellReuseIdentifier: "favoriteMovieCell")
        FavoriteMovies.loadFavoriteMovies()
        movies = FavoriteMovies.favoriteMoviesArray
        
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.navigationController?.navigationBar.topItem?.title = title
        FavoriteMovies.loadFavoriteMovies()
        movies = FavoriteMovies.favoriteMoviesArray
        tableFavoriteMovies.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FavoritesMoviesViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! FavoriteMovieTableViewCell
        
        
        performSegue(withIdentifier: "favoriteMovieCell", sender: self)
    }
    
}

extension FavoritesMoviesViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FavoriteMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "favoriteMovieCell") as! FavoriteMovieTableViewCell
        let movie = movies[indexPath.row]
        cell.lblTitle.text = movie.title
        cell.lblReleaseYear.text = movie.releaseYear
        cell.lblOverview.text = movie.overview
        ImageFromUrl.getImageFrom(urlString: "https://image.tmdb.org//t/p/w185/\(movie.posterPath!)") { (image) in
            DispatchQueue.main.async {
                cell.ivPoster.image = image
            }
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    
}

extension FavoritesMoviesViewController: UISearchBarDelegate{
    
}

