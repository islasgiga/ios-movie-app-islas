//
//  ViewController+CoreData.swift
//  MyMovies
//
//  Created by Islas Girão Garcia on 07/06/2018.
//  Copyright © 2018 Islas Girão Garcia. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension UIViewController{
    
    var context: NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}
